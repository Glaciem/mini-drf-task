from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Book, Library, LibraryBook


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'email']

class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ['id','name']

class LibrarySerializer(serializers.ModelSerializer):
    books = BookSerializer(many=True, read_only=False)

    def get_or_create_books(self, books):
        books_ids = []
        for book in books:
            book_instance, created = Book.objects.get_or_create(pk=book.get('id'), defaults=book)
            books_ids.append(book_instance.pk)
        return books_ids

    def update_or_create_books(self, books):
        books_ids = []
        for book in books:
            book_instance, created = Book.objects.update_or_create(pk=book.get('id'), defaults=book)
            books_ids.append(book_instance.pk)
        return books_ids

    def get_books(self, books):
        books_id = [book["id"] for book in books]
        return Book.objects.filter(id__in=books_id)

    def create(self, validated_data):
        books = validated_data.pop('books', [])
        library = Library.objects.create(**validated_data)
        library.books.set(self.get_or_create_packages(books))
        return library

    def update(self, instance, validated_data):
        books = validated_data.pop('books', [])
        instance.books.set(self.update_or_create_books(books))
        instance.save()
        return instance


    class Meta:
        model = Library
        fields = ['id','name', 'user', 'books']


class UserLibrariesSerializer(serializers.ModelSerializer):
    libraries = LibrarySerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'libraries']


class FavoriteBookCountSerializer(serializers.ModelSerializer):
    favorite_count = serializers.IntegerField()

    class Meta:
        model = Book
        fields = ['id', 'name', 'favorite_count']


class FavoriteLibraryCountSerializer(serializers.ModelSerializer):
    favorite_books_count = serializers.IntegerField() # КОММЕНТАРИЙ!!!!!
    #user_id = User.id



    # a bvsdFVSDFGDSFGS
    class Meta:
        model = Library
        fields = ['id', 'name', 'favorite_books_count']