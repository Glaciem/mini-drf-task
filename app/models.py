from django.db import models
from django.contrib.auth.models import User


class Book(models.Model):
    name = models.CharField('Название книги', max_length=500, null=False, unique=True)

    on_create = models.DateTimeField("Время создания", auto_now_add=True, editable=False, null=False)
    on_change = models.DateTimeField("Время изменения", auto_now=True, null=False)

    def __str__(self):
        return f"Book {self.name}"

    class Meta:
        verbose_name = "Книга"
        verbose_name_plural = "Книги"

class Library(models.Model):
    name = models.CharField('Название библиотеки', max_length=500, null=False, unique=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False, related_name='libraries')
    books = models.ManyToManyField(Book, through="LibraryBook", related_name='libraries')

    on_create = models.DateTimeField("Время создания", auto_now_add=True, editable=False, null=False)
    on_change = models.DateTimeField("Время изменения", auto_now=True, null=False)

    def __str__(self):
        return f"Library {self.name}"

    class Meta:
        verbose_name = "Библиотека"
        verbose_name_plural = "Библиотеки"


class LibraryBook(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    library = models.ForeignKey(Library, on_delete=models.CASCADE)
    favorite = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Книги библиотеки"
        verbose_name_plural = "Книги библиотек"