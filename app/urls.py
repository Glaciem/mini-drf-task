from django.urls import path, include
from rest_framework import routers
from . import views


app_name = "app"

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet, basename="users")
router.register(r'books', views.BookViewSet, basename="books")
router.register(r'libraries', views.LibraryViewSet, basename="libraries")
router.register(r'user_libraries', views.UserLibrariesViewSet, basename="user_libraries")


urlpatterns = [
    path('', include(router.urls)),
    path('book_to_favorite', views.BookToFavorite.as_view()),
    path('favorite_libraries', views.FavoriteLibrary.as_view()),
]