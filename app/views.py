from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import JsonResponse
from django.contrib.auth.models import User
from .models import Book, Library, LibraryBook
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import UserSerializer, BookSerializer, LibrarySerializer, \
    UserLibrariesSerializer, FavoriteBookCountSerializer, FavoriteLibraryCountSerializer
from rest_framework.decorators import action
from django.db.models import F, Q, Count, OuterRef, Subquery, Prefetch

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all().order_by('-on_create')
    serializer_class = BookSerializer
    permission_classes = [permissions.IsAuthenticated]


class LibraryViewSet(viewsets.ModelViewSet):
    queryset = Library.objects.prefetch_related('books').all().order_by('-on_create')
    serializer_class = LibrarySerializer
    permission_classes = [permissions.IsAuthenticated]


class UserLibrariesViewSet(viewsets.ModelViewSet):
    queryset = User.objects.prefetch_related('libraries').all().order_by('-date_joined')
    serializer_class = UserLibrariesSerializer
    permission_classes = [permissions.IsAuthenticated]


class BookToFavorite(APIView):
    permission_classes = [permissions.IsAdminUser]

    def post(self, request):
        """
        Помечает конкретную книгу в конкретной библиотеке как любимая
        """
        #print(request.query_params)
        library_id = request.GET.get("library", None)
        book_id = request.GET.get("book", None)

        if library_id and book_id:
            LibraryBook.objects.filter(book__id=book_id, library__id = library_id).update(favorite=True)
            return Response({"result": True})
        return Response({"result": False})

    def get(self, request):
        """
        Возвращает книги, которые были отмечены пользователем как любимые во всех библиотеках
        """
        books = (
            Book.objects
            .prefetch_related(
                Prefetch(
                    "librarybook_set",
                    queryset=LibraryBook.objects.filter(library__user=request.user).only("book")
                )
            )
            .annotate(
                favorite_count=Count(
                    F('librarybook__book__id'),
                    filter=Q(librarybook__favorite=True),
                )
            )
            .filter(favorite_count__gt=0)
            .only("name")
        )

        serializer = FavoriteBookCountSerializer(books, many=True)
        return Response(serializer.data)


class FavoriteLibrary(APIView):
    permission_classes = [permissions.IsAdminUser]

    def get(self, request):
        """
        Возвращает библиотеки пользователей, в которых есть хоть одна книга, помеченная как любимая
        """

        libraries = (
            Library.objects
            .select_related("user")
            .prefetch_related(
                Prefetch(
                    "librarybook_set",
                    queryset=LibraryBook.objects.filter(favorite=True).only("book", "library", "favorite")
                )
            )
            .annotate(
                favorite_books_count=Count(
                    F('librarybook__library__id'),
                    filter=Q(librarybook__favorite=True),
                )
            )
            .filter(favorite_books_count__gt=0)
            .only("name", "user__id")
        )

        serializer = FavoriteLibraryCountSerializer(libraries, many=True)
        return Response(serializer.data)
