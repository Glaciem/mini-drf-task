from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from . models import Book, Library

class BookLibraryInline(admin.TabularInline):
    model = Book.libraries.through

class LibraryBookInline(admin.TabularInline):
    model = Library.books.through


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    model = Book
    inlines = [BookLibraryInline]
    
    list_display = ['id', 'name', 'on_create', 'on_change']
    list_filter = ['on_create', 'on_change']
    search_fields = ['id', 'name']


@admin.register(Library)
class LibraryAdmin(admin.ModelAdmin):
    model = Library
    inlines = [LibraryBookInline]
    
    list_display = ['id', 'name', 'user', 'on_create', 'on_change']
    list_filter = ['on_create', 'on_change']
    search_fields = ['id', 'name', 'user__username', 'user__first_name', 'user__last_name']
